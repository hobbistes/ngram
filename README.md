# Ngram

* There is an one-time need to download 'punkt' from nltk. This doesn't need to happen from inside the program. You can just do it through a python shell. Download size ~ 13MB

```
import nltk
nltk.download('punkt')
```
