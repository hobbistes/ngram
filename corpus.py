import os
import re
import glob
import random
import string
import fileinput

from nltk import word_tokenize
from nltk import sent_tokenize

class Corpus(object):
    """Class that handles all operations related to the text corpus"""

    corpus = None
    dictionary         = {}
    sentences          = []
    unknown            = '*UNK*'
    pseudo_start       = '*start*'
    pseudo_start1      = '*start1*'
    pseudo_start2      = '*start2*'
    pseudo_end         = '*end*'
    size_of_vocabulary = 0

    def __init__(self):
        pass

    def _list_files(self):
        """Returns a list of all available files in the provided path"""
        return [
            file_path
            for file_path
            in glob.iglob(os.path.join(self.path, '**'), recursive=True)
            if re.search(self.pattern, file_path)
        ]

    def _process_input_line(self, line):
        """ Function that gets a text line removes html tags and stop words"""
        line = re.sub('<[^>]*>', '', line).replace('\n', '')
        return line

    def _calc_corpus(self, files):
        """ Get the total corpus of the provided files. """
        corpus = []
        _fileinput = fileinput.FileInput(files=files)

        for line in _fileinput:
            processed_line = self._process_input_line(line)
            if processed_line:
                corpus.append(processed_line)
        self.corpus = ' '.join(corpus)

    def _keep_n_frequent_words(self, n=10):
        """
            Keep a dictionary of the n most frequent words in the corpus.
        """
        words = word_tokenize(self.corpus)
        for word in words:
            word = word.lower()
            if word in string.punctuation:
                continue
            if word in self.dictionary:
                self.dictionary[word] += 1
            else:
                self.dictionary[word] = 1
        self.dictionary = {k:v for k,v in self.dictionary.items() if v >= n}
        self.size_of_vocabulary = len(self.dictionary)

    def _get_sentences(self, corpus):
        """ """
        return sent_tokenize(corpus)

    def _remove_unknown_words(self):
        """ """
        temp_corpus = []
        for word in word_tokenize(self.corpus):
            if word in string.punctuation:
                 temp_corpus.append(word)
            if word.lower() not in self.dictionary:
                temp_corpus.append(self.unknown)
            else:
                temp_corpus.append(word.lower())
        self.corpus = ' '.join(temp_corpus)

    def normalize_sentence(self, sentence):
        sentence = sentence.lower()
        temp_sentence = []
        for word in word_tokenize(sentence):
            if word in string.punctuation:
                continue
            if word not in self.dictionary:
                word = '*UNK*'
            temp_sentence.append(word)
        return ' '.join(temp_sentence)

    def generate_random_sentence(self, length):
        
        sentence = []
        for _ in range(length):
            sentence.append(random.choice(list(self.dictionary.keys())))
        return ' '.join(sentence)

    def get_bigram_sentences(self):

        ret_sentences = []
        for sentence in self.sentences:
            ret_sentences.append(self.pseudo_start + ' ' + sentence + ' ' + self.pseudo_end)
        #return ' '.join(ret_sentences)
        return ret_sentences

    def get_trigram_sentences(self):

        ret_sentences = []
        for sentence in self.sentences:
            ret_sentences.append(self.pseudo_start1 + ' ' + self.pseudo_start2 + ' ' + sentence + ' ' + self.pseudo_end)
        return ' '.join(ret_sentences)

    def load_corpus(self,files,is_training=False):
        """
            Public function that retrieves a large corpus of text form the
            initialized path.
        """
        if self.corpus == None:
            self._calc_corpus(files)

            # We are taking care of the dictionary only if we currently handle a training data
            if is_training:
                self._keep_n_frequent_words()
                self._remove_unknown_words()
                self.sentences = self._get_sentences(self.corpus)
                self.dictionary[self.pseudo_start] = len(self.sentences)
                self.dictionary[self.pseudo_start1] = len(self.sentences)
                self.dictionary[self.pseudo_start2] = len(self.sentences)
                self.dictionary[self.unknown] = 0
            else:
                self.sentences = self._get_sentences(self.corpus)

        return self.corpus
