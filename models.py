"""
TODO:

- We don't want to keep the (*start1*,*start2*) tuple for Trigrams! We currently do!
- Slide 8 - Are we doing this correctly? What if we have UNK in the numerator and not
  in the denominator? Have we considered this case?
"""
import math
import string

from nltk import word_tokenize

class Bigram(object):

    frequencies = {}
    probabilities = {}
    pseudo_start = '*start*'
    pseudo_end = '*end*'
    unknown = '*UNK*'

    def __init__(self, sentences, size_of_vocabulary, dictionary):
        """ Class constructor"""
        self.size_of_vocabulary = size_of_vocabulary
        self.dictionary = dictionary

        # Laplace Smoothing
        self.unknown_probability = 1 / size_of_vocabulary
        for sentence in sentences:
            sentence = self.pseudo_start + ' ' + sentence + ' ' + self.pseudo_end
            previous_word = None
            for word in word_tokenize(sentence):
                if word in string.punctuation:
                    continue
                if previous_word and word != self.unknown and previous_word != self.unknown:
                    self.frequencies[(previous_word,word)] = self.frequencies.get((previous_word,word), 0) + 1
                previous_word = word

    def train(self):
        """ Calculate probabilities with Laplace smoothing"""
        i = 0
        print("Vocabulary size: ", self.size_of_vocabulary)
        for key, value in self.frequencies.items():
            previous_word, _ = key if key[0] in self.dictionary else [self.unknown, ""]
            self.probabilities[key] = (value + 1) / (self.dictionary[previous_word] + self.size_of_vocabulary)
            if i < 10:
                print("Key: ", key, " Value: ", value, " Previous Word: ", previous_word, " Pr value: ", self.dictionary[previous_word])
                print("probability: ",self.probabilities[key])
                i += 1

    def calculate_sentence_probabilities(self, sentence):
        """ Calculates probability for a sentence """
        sentence = self.pseudo_start + ' ' + sentence + ' ' + self.pseudo_end
        # Since we are getting the log-probabilities, we begin with 0 and then
        # start adding up the log-probs.
        probability = 0.0
        previous_word = None
        for word in word_tokenize(sentence):
            if word in string.punctuation:
                continue
            if previous_word:
                if previous_word == self.unknown or word == self.unknown:
                    probability += math.log2(self.unknown_probability)
                else:
                    if (previous_word,word) in self.probabilities:
                        probability += math.log2( (self.probabilities[(previous_word,word)] ))
                    else:
                        probability += math.log2(1 / (self.dictionary[previous_word] + self.size_of_vocabulary))
            previous_word = word

        return math.pow(2, probability)

    def predict_next_word(self, sentence):
        """ Try to predict the next word given a sentence"""

        best_next_word = ''
        best_next_word_max_probability = 0.0
        for next_word in self.dictionary.keys():
            expanded_sentence = sentence + ' ' + next_word
            prediction_prob = self.calculate_sentence_probabilities(expanded_sentence)
            if prediction_prob > best_next_word_max_probability:
                best_next_word = next_word
                best_next_word_max_probability = prediction_prob

        return (best_next_word, best_next_word_max_probability)

    def calculate_cross_entropy_perplexity(self, sentence):
        """ Calculate the cross entropy for this model"""

        probability = 0.0
        previous_word = None
        words = word_tokenize(sentence)
        for word in words:
            if word in string.punctuation:
                continue
            if previous_word:
                if previous_word == self.unknown or word == self.unknown:
                    probability += math.log2(self.unknown_probability)
                elif word == self.pseudo_start:
                    pass
                else:
                    if (previous_word,word) in self.probabilities:
                        probability += math.log2(self.probabilities[(previous_word,word)])
                    else:
                        probability += math.log2(1 / (self.dictionary[previous_word] + self.size_of_vocabulary))
            previous_word = word
            cross_entropy = - (probability / len(words))
            perplexity = math.pow(2, cross_entropy)
        return  (cross_entropy, perplexity)


class Trigram(object):

    frequencies = {}
    probabilities = {}
    pseudo_start1 = '*start1*'
    pseudo_start2 = '*start2*'
    pseudo_end = '*end*'
    unknown = '*UNK*'

    def __init__(self, sentences, size_of_vocabulary, dictionary):
        """ Class constructor"""
        self.size_of_vocabulary = size_of_vocabulary
        self.dictionary = dictionary

        # Laplace Probability
        self.unknown_probability = 1 / size_of_vocabulary

        for sentence in sentences:
            sentence = self.pseudo_start1 + ' ' + self.pseudo_start2 + ' ' + sentence + ' ' + self.pseudo_end
            previous_word1 = None
            previous_word2 = None
            for word in word_tokenize(sentence):
                if word in string.punctuation:
                    continue
                # Do we really need this??
                if previous_word2 and word != self.unknown and previous_word2 != self.unknown:
                    self.frequencies[(previous_word2,word)] = self.frequencies.get((previous_word2,word), 0) + 1
                if previous_word1 and previous_word2 and word != self.unknown and previous_word1 != self.unknown and previous_word2 != self.unknown:
                    self.frequencies[(previous_word1,previous_word2,word)] = self.frequencies.get((previous_word1,previous_word2,word), 0) + 1
                previous_word1 = previous_word2
                previous_word2 = word

    def train(self):
        """ Calculate probabilities with Laplace smoothing"""
        """
            We are considering that the bigrams of the form ('*UNK*',known_word) and (known_word,'*UNK*')
            will be treated as unknown and thus have a probability of 1/dictionary_length.
            From wikipedia:
                An issue when using n-gram language models are out-of-vocabulary (OOV) words.
                They are encountered in computational linguistics and natural language processing
                when the input includes words which were not present in a system's dictionary
                or database during its preparation. By default, when a language model is estimated,
                the entire observed vocabulary is used. In some cases, it may be necessary to
                estimate the language model with a specific fixed vocabulary. In such a scenario,
                the n-grams in the corpus that contain an out-of-vocabulary word are ignored.
        """
        for key, value in self.frequencies.items():
            if len(key) == 3:
                previous_word1, previous_word2, _ = key
                if previous_word1 not in self.dictionary:
                    previous_word1 = self.unknown
                if previous_word2 not in self.dictionary:
                    previous_word2 = self.unknown
                if previous_word1 == self.unknown or previous_word2 == self.unknown:
                    self.probabilities[key] = (value + 1) / self.size_of_vocabulary
                else:
                    self.probabilities[key] = (value + 1) / (self.frequencies[(previous_word1,previous_word2)] + self.size_of_vocabulary)
            # Do we really need this? Why??
            # if len(key) == 2:
            #     previous_word, _ = key if key[0] in self.dictionary else [self.unknown, ""]
            #     self.probabilities[key] = (value + 1) / (self.dictionary[previous_word] + self.size_of_vocabulary)

    def calculate_sentence_probabilities(self, sentence):
        """ Calculates probability for a sentence """
        sentence = self.pseudo_start1 + ' ' + self.pseudo_start2 + ' ' + sentence + ' ' + self.pseudo_end
        probability = 0.0
        previous_word1 = None
        previous_word2 = None
        for word in word_tokenize(sentence):
            if word in string.punctuation:
                continue
            if previous_word1 and previous_word2:
                if previous_word1 == self.unknown or previous_word2 == self.unknown or word == self.unknown:
                    probability += math.log2(self.unknown_probability)
                else:
                    if (previous_word1,previous_word2,word) in self.probabilities:
                        probability += math.log2(self.probabilities[(previous_word1,previous_word2,word)])
                    else:
                        if (previous_word1,previous_word2) in self.frequencies:
                            probability += math.log2(1 / (self.frequencies[(previous_word1,previous_word2)] + self.size_of_vocabulary))
                        else:
                            probability += math.log2(1 / self.size_of_vocabulary)
            previous_word1 = previous_word2
            previous_word2 = word

        return math.pow(2, probability)

    def predict_next_word(self, sentence):
        """ Try to predict the next word given a sentence"""

        best_next_word = ''
        best_next_word_max_probability = 0.0
        for next_word in self.dictionary.keys():
            expanded_sentence = sentence + ' ' + next_word
            prediction_prob = self.calculate_sentence_probabilities(expanded_sentence)
            if prediction_prob > best_next_word_max_probability:
                best_next_word = next_word
                best_next_word_max_probability = prediction_prob

        return (best_next_word, best_next_word_max_probability)

    def calculate_cross_entropy_perplexity(self, sentence):
        """ Calculate the cross entropy for this model"""

        probability = 0.0
        previous_word1 = None
        previous_word2 = None
        words = word_tokenize(sentence)
        for word in word_tokenize(sentence):
            if word in string.punctuation:
                continue
            if previous_word1 and previous_word2:
                if previous_word1 == self.unknown or previous_word2 == self.unknown or word == self.unknown:
                    probability += math.log2(self.unknown_probability)
                else:
                    if (previous_word1,previous_word2,word) in self.probabilities:
                        probability += math.log2(self.probabilities[(previous_word1,previous_word2,word)])
                    else:
                        if (previous_word1,previous_word2) in self.frequencies:
                            probability += math.log2(1 / (self.frequencies[(previous_word1,previous_word2)] + self.size_of_vocabulary))
                        else:
                            probability += math.log2(1 / self.size_of_vocabulary)
            previous_word1 = previous_word2
            previous_word2 = word
            cross_entropy = - (probability / len(words))
            perplexity = math.pow(2, cross_entropy)
        return  (cross_entropy, perplexity)


class LinearInterpolation(object):

    pseudo_start1 = '*start1*'
    pseudo_start2 = '*start2*'
    pseudo_end    = '*end*'
    unknown       = '*UNK*'

    def __init__(self, uni_dict, bi_probs, tri_probs, lambdas):
        """ Class constructor"""
        self.uni_dict            = uni_dict
        self.bi_probs            = bi_probs
        self.tri_probs           = tri_probs
        self.lambdas             = lambdas
        self.size_of_vocabulary  = len(uni_dict)
        self.unknown_probability = 1 / self.size_of_vocabulary

    def calculate_sentence_probabilities(self, sentence):
        """ Calculates probability for a sentence """
        sentence = self.pseudo_start1 + ' ' + self.pseudo_start2 + ' ' + sentence + ' ' + self.pseudo_end
        uni_prob = 0.0
        bi_prob  = 0.0
        tri_prob = 0.0
        probability = 0.0

        # Get the first 2 words outside of the loop
        sentence_tokens  = word_tokenize(sentence)
        previous_word1   = sentence_tokens[0]
        previous_word2   = sentence_tokens[1]
        previous_word_bi = '*start*'

        for word in sentence_tokens[2:]:
            if word in string.punctuation:
                continue

            # Probability part coming from the trigram
            if (previous_word1,previous_word2,word) not in self.tri_probs.keys():
                tri_prob += math.log2(self.unknown_probability)
            else:
                tri_prob += math.log2(self.tri_probs[(previous_word1,previous_word2,word)])

            # Probability part coming from the bigram
            if (previous_word_bi,word) not in self.bi_probs.keys():
                bi_prob += math.log2(self.unknown_probability)
            else:
                bi_prob += math.log2(self.bi_probs[previous_word_bi,word])

            # Probability part coming from the unigram
            if word == "*UNK*" or word not in self.uni_dict.keys():
                uni_prob += self.unknown_probability
            else:
                uni_prob += math.log2(self.uni_dict[word]/len(self.uni_dict))

            previous_word_bi = word
            previous_word1   = previous_word2
            previous_word2   = word

        uni_prob = math.pow(2,uni_prob) * self.lambdas[0]
        bi_prob  = math.pow(2,bi_prob)  * self.lambdas[1]
        tri_prob = math.pow(2,tri_prob) * self.lambdas[2]

        return uni_prob + bi_prob + tri_prob